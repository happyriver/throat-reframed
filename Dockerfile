FROM clojure:openjdk-17-tools-deps-slim-buster AS build-jar
RUN apt update && apt install -y curl
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install -y nodejs && npm install clean-css-cli -g

# Build the JavaScript and other assets.
FROM build-jar as build-assets
WORKDIR /build/throat-fe
COPY throat-fe/package.json throat-fe/package-lock.json ./
RUN npm install
COPY throat-fe/shadow-cljs.edn throat-fe/deps.edn ./
RUN npx shadow-cljs
COPY throat-fe .
RUN npm run release

FROM build-assets as build-minify
# Minify the CSS
WORKDIR /build/throat-fe/resources/public/css
COPY Dockerfile custom/custom.css* ./
RUN cleancss -o throat-fe.min.css pikaday/pikaday.css tachyons.css main.css dark.css throat-fe.css custom.css

# Build the uberjar.
FROM clojure:openjdk-17-lein-slim-buster as build-uberjar
WORKDIR /build/throat-be
COPY throat-be/project.clj .
RUN lein deps
COPY throat-be .
RUN rm -f resources/logback.xml; lein uberjar

# Copy build results into an image without the build tools.
FROM eclipse-temurin:17-jre-alpine

WORKDIR /install
COPY --from=build-uberjar /build/throat-be/target/uberjar/throat-be-0.1.0-SNAPSHOT-standalone.jar app.jar
COPY --from=build-minify /build/throat-fe/resources/public/css/throat-fe.min.css shared/fe/css/
COPY --from=build-assets /build/throat-fe/resources/public/css/purecss shared/fe/css/purecss/
COPY --from=build-assets /build/throat-fe/resources/public/js shared/fe/js/
COPY --from=build-assets /build/throat-fe/resources/public/img shared/fe/img/

# Copy any post-build customizations into the install
# directory. Copying the Dockerfile and custom* allows the custom
# directory to not be present.
COPY throat-be/resources/logback.xml /install/
COPY Dockerfile custom* /install/

# Calculate a hash of the static files and use it to give them a
# cachebusting directory name.
RUN mv shared/fe shared/$(find shared/fe -type f -print0 | \
                          xargs -0 sha1sum | sha1sum | \
                          head -c 10)

RUN addgroup -S app && adduser -S app -G app
USER app
EXPOSE 8888

ENV ASSET_PATH=/install/shared

ENTRYPOINT ["java", "-Dlogback.configurationFile=/install/logback.xml", "-jar", "app.jar"]
CMD ["--webserver", "--taskrunner"]
